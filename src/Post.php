<?php

class Post
{
    /**
     * @var Article
     */
    private $article;

    /**
     * @var string
     */
    const POSTED_IN = 'Digitalia';

    public function __construct($article)
    {
        $this->article = $article;
    }

    /**
     * Checks to see if the post is correct according to the post we are looking for
     *
     * @return boolean
     */
    public function isCorrectPost()
    {
        $postCategory = $this->article->find('a[rel=tag]');
        if ($postCategory[0]->text() == self::POSTED_IN) {
            return true;
        }

        return false;
    }

    /**
     * Gets the url and link for the post
     *
     * @return array
     */
    public function getPostData()
    {
        $data = $this->article->find('div.entry-summary > ul > li > a');

        return [
            'url' => $data[0]->attr('href'),
            'link' => $data[0]->text()
        ];
    }
}
