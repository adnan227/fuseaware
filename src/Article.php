<?php

class Article
{
    /**
     * @var DomDocument
     */
    private $document;

    public function __construct($document)
    {
        $this->document = $document;
    }

    /**
     * Gets all the articles from the main page
     */
    public function getArticles()
    {
        return $this->document->find('.post');
    }
}
