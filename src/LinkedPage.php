<?php

class LinkedPage
{
    /**
     * @var string the url of the linked page
     */
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Gets the size of the file
     *
     * @return integer
     */
    public function getFileSize()
    {
        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($curl);
        $size = curl_getinfo($curl, CURLINFO_SIZE_DOWNLOAD);

        return ($size / 1000);
    }

    /**
     * Gets the meta data for the linked page
     *
     * @return array
     */
    public function getMetaData()
    {
        $document = new DomDocument();
        @$document->loadHTML(file_get_contents($this->url));

        $description = $keyword = '';

        foreach ($document->getElementsByTagName('meta') as $meta) {
            if ($meta->hasAttribute('name')) {
                if ($meta->getAttribute('name') == 'description') {
                    $description = $meta->getAttribute('content');
                }
            }

            if ($description == '') {
                if($meta->getAttribute('property') == 'og:description') {
                    $description = $meta->getAttribute('content');
                }
            }

            if ($meta->hasAttribute('name')) {
                $keywword = $meta->getAttribute('content');
            }
        }

        $metaTagInfo = [
            'description' => $description,
            'keyword' => $keyword
        ];

        return $metaTagInfo;
    }
}
