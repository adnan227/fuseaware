<?php
/**
 *
 */
class ResultData
{
    /**
     * @var array holds the dataset for results
     */
    private $resultData;

    /**
     * @var integer the total size of all the pages
     */
    private $totalFileSize;

    public function __construct()
    {
        $this->resultData = $this->setDefaultDataSet();
    }

    /**
     * Appends the dataset to the full array
     *
     * @param array $getData The url and link for the result dataset
     * @param array $metaTag The Description and Keyword for the result dataset
     * @param integer $fileSize The size of the external page
     *
     * @return boolean
     */
    public function addResultToDataset($getData, $metaTag, $fileSize)
    {
        $this->resultData['results'][] = [
            'url'               => $getData['url'],
            'link'              => $getData['link'],
            'meta description'  => $metaTag['description'],
            'keywords'          => $metaTag['keyword'],
            'file_size'         => $fileSize . 'Kb'
        ];

        return true;
    }

    /**
     * Increases the total file size of all pages
     *
     * @param integer $fileSize The amount to add on to the total
     */
    public function increaseTotalFileSize($fileSize)
    {
        return $this->totalFileSize += $fileSize;
    }

    /**
     * Returns the result dataset as a json
     *
     * @return string
     */
    public function printAsJson()
    {
        return json_encode($this->resultData);
    }

    /**
     * Getter for the result data
     *
     * @return array
     */
    public function getData()
    {
        return $this->resultData;
    }

    public function getTotalFileSize()
    {
        return $this->totalFileSize;
    }

    /**
     * Adds the total file size to the results data
     */
    public function addTotalToData()
    {
        return $this->resultData['total'] = $this->totalFileSize . 'Kb';
    }

    /**
     * Creates the default array structure
     *
     * @return true
     */
    private function setDefaultDataSet()
    {
        $this->totalFileSize = 0;
        $this->resultData = [
            'results' => [],
            'total' => ''
        ];
    }
}
