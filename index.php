<?php
require_once('vendor/autoload.php');
require_once('src/Autoloader.php');

$document = new DiDom\Document('http://www.black-ink.org', true);
$resultData = new ResultData();
$article = new Article($document);
$articles = $article->getArticles();

foreach ($articles as $article) {

    $post = new Post($article);

    if ($post->isCorrectPost()) {
        $getData = $post->getPostData();

        $linkedPage = new LinkedPage($getData['url']);
        $fileSize = $linkedPage->getFileSize();
        $metaData = $linkedPage->getMetaData();

        $resultData->addResultToDataset($getData, $metaData, $fileSize);
        $resultData->increaseTotalFileSize($fileSize);
    }
}

$resultData->addTotalToData();
$json = $resultData->printAsJson();
print($json);
?>
