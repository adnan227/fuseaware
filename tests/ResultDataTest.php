<?php
require_once('src/ResultData.php');

use PHPUnit\Framework\TestCase;

final class ResultDataTest extends TestCase
{
    public function testAddResultToDataset()
    {
        $resultData = new ResultData();
        $resultDataset = $resultData->getData();

        // test that it is empty
        $this->assertEmpty($resultDataset['results']);
        $this->assertEquals($firstTimeTotalFilesize, 0);

        // add the dataset
        $dataset = $this->dataProvider();
        $resultData->addResultToDataset($dataset['getData'], $dataset['metaTag'], 123);

        // get the new data
        $resultDatasetAdded = $resultData->getData();

        // test that it is no longer empty
        $this->assertNotEmpty($resultDatasetAdded['results']);
    }

    public function testIncreaseTotalFileSize()
    {
        $resultData = new ResultData();
        $firstTimeTotalFilesize = $resultData->getTotalFileSize();

        // test that the total filesize is empty
        $this->assertEquals($firstTimeTotalFilesize, 0);

        // increase the amount total filesize
        $resultData->increaseTotalFileSize(123);
        $newTotalFileSize = $resultData->getTotalFileSize();

        // test that the original filesize and the new total filesize aren't the same
        $this->assertNotEquals($firstTimeTotalFilesize, $newTotalFileSize);

        // the new total filesize should be 123
        $this->assertEquals($newTotalFileSize, 123);
    }

    public function testPrintAsJson()
    {
        $resultData = new ResultData();

        // add the dataset
        $dataset = $this->dataProvider();
        $resultData->addResultToDataset($dataset['getData'], $dataset['metaTag'], 123);
        $resultData->increaseTotalFileSize(123);
        $resultData->addTotalToData();

        // get the new data
        $resultDatasetAdded = $resultData->getData();

        // get json data
        $json = $resultData->printAsJson();

        // test that it is no longer empty and that the structure is correct
        $this->assertNotEmpty($resultDatasetAdded['results']);
        $this->assertJsonStringEqualsJsonFile('tests/defaultFileStructure.txt', $json);
    }

    private function dataProvider()
    {
        return [
            'getData' => [
                'url'   => 'http://www.fuseaware.co.uk',
                'link'  => 'fuseAware is an app for workers, and a dashboard for employers, that provides real time data'
            ],
            'metaTag' => [
                'description'  => 'Test meta description',
                'keyword'          => 'test, keywords',
            ]
        ];
    }
}

?>
