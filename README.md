fuseAware
======

This code is the solution for Adnan Ahmad for the technical test for fuseAware

Installing
======

This project has some requirements that need to be installed via Composer. To install, simply run:

```
composer install
```

You can download Composer at: https://getcomposer.org/download/

Running the script
======

The output can be seen by loading the url in the browser after installing via composer or running

```
php index.php
```

Running the Tests
======

The tests have been written in PHPUnit and so to run them (assuming you have installed PHPUnit):
1. Go to root directory
2. Run ```phpunit tests/```
